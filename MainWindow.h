#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QTimer>

#include <SDR/Qt_Addons/TScopePlot.h>
#include <SDR/Qt_Addons/TSpectrumPlot.h>
#include <SDR/Qt_Addons/TBufferPlot.h>
#include <SDR/Qt_Addons/TScatterPlot.h>

#include <ReceiverSignalsNavdatInterface.h>
#include <ReceiverSignalsInputDriver.h>

namespace Ui {
class MainWindow;
}

class TSoprocessor : public QObject
{
  Q_OBJECT
public:
  explicit TSoprocessor(QThread* thread = 0, QObject *parent = 0);
  ~TSoprocessor();

  void setScopePlot(SDR::TScopePlot* plot){ScopePlot = plot;}
  void setSpectrumPlot(SDR::TSpectrumPlot* plot){SpectrumPlot = plot;}
  void setSyncFreqEstimationPlot(SDR::TBufferPlot* plot){SyncFreqEstimationPlot = plot;}
  void setSyncTettaPlot(SDR::TBufferPlot* plot){SyncTettaPlot = plot;}
  void setEqualizerPlot(SDR::TBufferPlot* plot){EqualizerPlot = plot;}
  void setPhasePlot(SDR::TBufferPlot* plot){PhasePlot = plot;}
  void setScatterPlot(SDR::TScatterPlot* plot){ScatterPlot = plot;}

private:
  Frequency_t Fd;
  Size_t Nfft;

  SDR::TScopePlot* ScopePlot;
  SDR::TSpectrumPlot* SpectrumPlot;
  SDR::TBufferPlot *SyncFreqEstimationPlot, *SyncTettaPlot, *EqualizerPlot, *PhasePlot;
  SDR::TScatterPlot* ScatterPlot;

private slots:
  void setConfigData(quint32 Fd, quint32 Nfft);

  void setScopeData(QVector<float> iqSamples);
  void setSyncEstimationsData(QVector<float> corr, QVector<float> freq);
  void setSpectrumData(QVector<float> iqSamples);
  void setEqualizerData(QVector<float> Samples);
  void setPhaseOffsetData(QVector<float> Samples);
  void setConstellationData(QVector<float> iqSamples);
};

template<typename T>
struct NAVDAT_FrameTemplate
{
  QVector<T> misData, tisData, dsData;
};

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

signals:
  void setSignalParams(quint8 signalType, ReceiverSignalsInterface::CommonSignalParams params, QByteArray spec_data);
  void setSignalEnabled(quint8 type, bool isEnabled);
  void disableDevice();
  void disableNavdatIf();

private:
  enum RefreshState {rs_Disable, rs_Waiting, rs_Updated};
  Ui::MainWindow *ui;

  bool IsWaitConnection;
  RefreshState stateRefresh;
  ReceiverSignalsNavdatInterface navdatIf;
  ReceiverSignalsInputDriver Driver;

  Frequency_t Fd;
  Size_t Nfft;

  QTimer refreshTimer;
  TSoprocessor Soprocessor;
  SDR::TScopePlot* ScopePlot;
  SDR::TSpectrumPlot* SpectrumPlot;
  SDR::TBufferPlot *SyncFreqEstimationPlot, *SyncCorrelationPlot, *EqualizerPlot, *PhasePlot;
  SDR::TScatterPlot* ScatterPlot;

  QString measurementFileName;
  qint8 measurement_tisQamBitsCount, measurement_dsQamBitsCount;
  NAVDAT_FrameTemplate<qint8> measurementMER;
  NAVDAT_FrameTemplate<quint8> measurementBER0, measurementBER;

  void message(QString header, QString msg, int timeout=30000);

  void init_plots();
  void ui_setConnected(bool flag);

  void setSignalParams(quint8 signal, QByteArray spec_data=0);
  void setMeasurementParams(quint8 symbol);

  void setSignalsParams();
  void setMeasurmentsParams();

  void enableSignals(bool isRequest = false);
  void setSignalsEnabled(bool enabled);

  void enableMeasurements();
  void setMeasurementsEnabled(bool enabled);

  Size_t getScopeSamplesCount(){return ScopePlot->getRefreshInterval()*Fd;}

private slots:
  void ui_clearWidgets();

  void refreshRequest();
  void setRefreshStateDisable();
  void setRefreshStateWaiting();
  void setRefreshStateUpdated();

  void clearPlots();
  void clearPlotsData();

  void openConnection();
  void closeConnection();

  void setConfigData(quint32 Fd, quint32 Nfft);
  void setSignalDetectedData(quint32 startIdx);
  void setEstimationsData(float snr, float freqOffset);

  void setQamData(quint8 xS, QVector<qint8> data);
  void setEncData(quint8 xS, QVector<quint8> data);
  void setData(quint8 xS, QVector<quint8> data);

  void navdatIf_enabled_handler();
  void navdatIf_disabled_handler();

  void setScopeParams();

  void navdat_enable_scope();
  void navdat_disable_scope();
  void navdat_enable_spectrum();
  void navdat_disable_spectrum();
  void navdat_enable_equalizer();
  void navdat_disable_equalizer();
  void navdat_enable_phase_offset();
  void navdat_disable_phase_offset();
  void navdat_enable_constellation();
  void navdat_disable_constellation();
  void navdat_enable_sync_estimations();
  void navdat_disable_sync_estimations();

  void on_button_connectionControl_clicked();
  void on_action_outputEnableAll_triggered();
  void on_action_outputDisableAll_triggered();
  void on_action_outputScope_triggered(bool enabled);
  void on_action_outputSpectrum_triggered(bool enabled);
  void on_action_outputSyncEstimations_triggered(bool enabled);
  void on_action_outputEqualizer_triggered(bool enabled);
  void on_action_outputPhaseOffset_triggered(bool enabled);
  void on_action_outputConstellation_triggered(bool enabled);
  void on_action_outputSyncStartIdx_triggered(bool enabled);
  void on_action_outputEstimations_triggered(bool enabled);
  void on_action_outputMER_triggered(bool enabled);
  void on_action_outputBER0_triggered(bool enabled);
  void on_action_outputBER_triggered(bool enabled);
  void on_action_outputEnableMeasurement_triggered();
  void on_action_outputDisableMeasurement_triggered();
  void on_action_measurementSetFile_triggered();
  void on_tabSignals_currentChanged(int index);
  void on_RefreshTime_valueChanged(double arg1);
  void on_ScopeTime_valueChanged(double arg1);
};

#endif // MAINWINDOW_H
