#
# \file sdr.pri
# \author Шиганцов А.А.
#

QMAKE_CFLAGS += -std=gnu11

win32 {
    SDR_HEADERS += $$system(dir /S /B /A:-D *.h*)
    for(file, SDR_HEADERS) HEADERS += $$file

    SDR_SOURCES += $$system(dir /S /B /A:-D *.c*)
    for(file, SDR_SOURCES) SOURCES += $$file
}

unix {
    SDR_HEADERS += $$system(find $$PWD -name \*.h\*)
    for(file, SDR_HEADERS) HEADERS += $$file

    SDR_SOURCES += $$system(find $$PWD -name \*.c\*)
    for(file, SDR_SOURCES) SOURCES += $$file
}

INCLUDEPATH += $$PWD $$PWD/port_gsl-liquid/include $$PWD/Qt_Addons

LIBS += -lgsl -lgslcblas -lm -lliquid
