#include <qglobal.h>

extern "C" void sdr_assert(const char *assertion, const char *file, int line)
{
  qt_assert(assertion, file, line);
}
