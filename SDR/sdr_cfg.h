﻿/*!
 * \file sdr.h
 * \author Андрей Шиганцов
 * \brief 
 */

#ifndef SDR_H_
#define SDR_H_

#ifdef __cplusplus
extern "C"{
#endif

void sdr_assert(const char *assertion, const char *file, int line);

#ifdef __cplusplus
}
#endif

#define SDR_ASSERT(cond) if (!(cond)) sdr_assert(#cond,__FILE__,__LINE__)

#endif /* SDR_H_ */
