#include "MainWindow.h"
#include <QApplication>

static void setIzvayanieUiTheme(QApplication &application) {
    application.setStyle("Fusion");
    QPalette palette; QColor base = QColor(21,21,21);
    palette.setColor(QPalette::Window, base);
    palette.setColor(QPalette::WindowText, Qt::white);
    palette.setColor(QPalette::Base, base);
    palette.setColor(QPalette::AlternateBase, base);
    palette.setColor(QPalette::ToolTipBase, Qt::white);
    palette.setColor(QPalette::ToolTipText, Qt::white);
    palette.setColor(QPalette::Text, Qt::white);
    palette.setColor(QPalette::Button, base);
    palette.setColor(QPalette::ButtonText, Qt::white);
    palette.setColor(QPalette::BrightText, Qt::red);
    palette.setColor(QPalette::Highlight, QColor(39,76,118));
    palette.setColor(QPalette::HighlightedText, Qt::white);
    palette.setColor(QPalette::Disabled, QPalette::Text, Qt::darkGray);
    palette.setColor(QPalette::Disabled, QPalette::ButtonText, Qt::darkGray);
    application.setPalette(palette);
    application.setStyleSheet("QLabel{color:rgb(200,200,128)}");
}

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  setIzvayanieUiTheme(a);

  MainWindow w;
  w.show();

  return a.exec();
}
